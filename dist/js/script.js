
const burgerMenu = document.querySelector('.burger-menu')
const burgerMenuOpen = document.querySelector('.burger-menu-open')
const burgerMenuClose = document.querySelector('.burger-menu-close')
const ul = document.querySelector('ul')
const openImg = document.querySelector('.burger-menu-open img')
const closeImg = document.querySelector('.burger-menu-close img')

burgerMenu.addEventListener('click', (e) => {
    if (e.target === openImg) {
        ul.classList.add('visibly')
        burgerMenuClose.classList.add('active')
    } else if (e.target === closeImg) {
        burgerMenuOpen.classList.add('active')
        ul.classList.remove('visibly')
        burgerMenuClose.classList.remove('active')
    }
})